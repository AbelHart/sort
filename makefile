#https://www.youtube.com/watch?v=aw9wHbFTnAQ 

# makefile version 1.0.09.08.2018

CFLAGS=-std=c++17 -c -g -Wall

	
sort: sort.o
	@printf "\033[36mLinking \"sort\"...\n\033[0m"
	g++ sort.o -o sort.out
	@printf "\n\033[34mRun by typing 'make run'\n\n\033[0m"

sort.o: sort.cpp
	@printf "\033[36mCompiling \"sort\"...\n\033[0m"
	g++ $(CFLAGS) sort.cpp

# ...................................................................

# generate the data files needed 
datafiles:
	@bash gendata
# ...................................................................

# extract the data needed 
data:
	@tar xvf data.tar.gz
# ...................................................................

results: sort.o
	@printf "\033[35mGenerating results \033[33mdata.csv\n\033[0m"
	$(shell bash gencsv > data.csv)
# ...................................................................

run:
	@./sort.out

# ...................................................................

# remove temp files

clean:
	rm -f *.out *.o 


